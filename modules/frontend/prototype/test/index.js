const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox']
  });
  const page = await browser.newPage();
  await page.goto('https://google.com');
  await page.screenshot({path: 'screenshots/google.png'});

  await browser.close();
})();
